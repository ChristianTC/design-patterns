﻿
using Microsoft.AspNetCore.Mvc;
using DesignPatterns.Model;

namespace DesignPatterns.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MotorController: ControllerBase
    {
        [HttpGet]
        [Route("diesel")]
        public IActionResult GetMotorsDiesel()
        {
            return Ok(MotorDiesel.GetDiesel());
        }
        [HttpGet]
        [Route("gasoline")]
        public IActionResult GetMotorsGasoline()
        {
            return Ok(MotorGasoline.GetGasoline());
        }
        [HttpGet]
        [Route("electric")]
        public IActionResult GetMotorsElectric()
        {
            return Ok(MotorElectricAdapter.GetElectric());
        }
    }
}
