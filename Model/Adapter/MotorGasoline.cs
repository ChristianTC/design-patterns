﻿namespace DesignPatterns.Model
{
    public class MotorGasoline : Motor
    {
        public MotorGasoline(int Amount)
        {
            this.Amount = Amount;
            Status = Refuel();
        }
        public int? Amount { get; set; }
        public string? Status { get; set; }
        public override void Start()
        {
            Console.WriteLine("Start - gasoline");
        }
        public override void Stop()
        {
            Console.WriteLine("Stop - gasoline");
        }
        public override string Refuel()
        {
            return ("Refuel - gasoline");
        }
        public override void SpeedUp()
        {
            Console.WriteLine("SpeedUp - gasoline");
        }

        private static class MotorHolder
        {
            public static List<MotorGasoline> INSTANCE = new List<MotorGasoline>{
                new MotorGasoline(11212),
                new MotorGasoline(12120)
            };
        }

        public static List<MotorGasoline> GetGasoline()
        {
            return MotorHolder.INSTANCE;
        }
    }
}
