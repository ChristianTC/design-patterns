﻿namespace DesignPatterns.Model
{
    public class MotorElectricAdapter : Motor
    {
        public MotorElectricAdapter()
        {
            Status = Refuel();
        }
        public string? Status { get; set; }


        MotorElectric motorElectric = new MotorElectric();

        public override void Start()
        {
            motorElectric.Move();
        }
        public override void Stop()
        {
            motorElectric.Connect();
            motorElectric.Activate();
        }
        public override string Refuel()
        {
            return motorElectric.Plug();
        }
        public override void SpeedUp()
        {
            motorElectric.Desactivate();
            motorElectric.Stop();
        }

        private static class MotorHolder
        {
            public static List<MotorElectricAdapter> INSTANCE = new List<MotorElectricAdapter>{
                new MotorElectricAdapter(),
                new MotorElectricAdapter()
            };
        }

        public static List<MotorElectricAdapter> GetElectric()
        {
            return MotorHolder.INSTANCE;
        }
    }
}
