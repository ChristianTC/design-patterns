﻿namespace DesignPatterns.Model
{
    public abstract class Motor
    {
        public abstract void Start();
        public abstract void Stop();
        public abstract void SpeedUp();
        public abstract string Refuel();

    }
}
