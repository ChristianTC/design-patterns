﻿namespace DesignPatterns.Model
{
    public class MotorDiesel: Motor
    {
        public MotorDiesel(int Amount)
        {
            this.Amount = Amount;
            Status = Refuel();
        }
        public int? Amount { get; set; }
        public string? Status{ get; set; }

        public override void Start()
        {
            Console.WriteLine("Start - Diesel");
        }
        public override void Stop()
        {
            Console.WriteLine("Stop - Diesel");
        }
        public override string Refuel()
        {
            return ("Refuel - Diesel");
        }
        public override void SpeedUp()
        {
            Console.WriteLine("SpeedUp - Diesel");
        }

        private static class MotorHolder
        {
            public static List<MotorDiesel> INSTANCE = new List<MotorDiesel>{
                new MotorDiesel(12),
                new MotorDiesel(120)
            };
        }

        public static List<MotorDiesel> GetDiesel()
        {
            return MotorHolder.INSTANCE;
        }
    }
}
