﻿namespace DesignPatterns.Model
{
    public class MotorElectric
    {
        bool _connected;
        bool _active;
        bool _moving;

        public string Connect()
        {
            if (_connected == true)
            {
                return ("Imposible to connect a electric motor already connected");
            }
            else
            {
                _connected = true;
                return ("Motor connected");
            }
        }

        public string Activate()
        {
            if (!_connected)
            {
                return ("Imposible to activate a motor no connected");
            }
            else
            {
                _active = true;
                return ("Motor actived");
            }
        }

        public string Move()
        {
            if (_connected && _active)
            {
                _moving = true;
                return ("Moving car with a electric motor");
            }
            else
            {
                return ("The motor must be connected and actived");
            }
        }
        public string Stop()
        {
            if (_moving)
            {
                _moving = false;
                return ("Stopping the car with electric motor");
            }
            else
            {
                return ("It can't stop because it is not moving");
            }
        }
        public string Disconnect()
        {
            if (_connected)
            {
                return ("Motor disconnected");
            }
            else
            {
                return ("It can't stop because it is not connected");
            }
        }
        public string Desactivate()
        {
            if (!_active)
            {
                _active = false;
                return ("Motor loading the batteries");
            }
            else
            {
                return ("It can't plug because it is not active");
            }
        }

        public string Plug()
        {
            if (!_active)
            {
                _active = false;
                return ("Motor loading the batteries");
            }
            else
            {
                return ("Imposible plug a motor actived");
            }
        }

        
    }
}
