# DesignPatterns

Steps to execute the App
1. Clone this repository
2. Keep in the branch main
3. Execute 

![image-2.png](./image-2.png)

4. After that open link https://localhost:7051/swagger/index.html

![image-1.png](./image-1.png)



## NOTES
PATTERN ADAPTER

In this project the API to refuel motor types

Motor types:
* Diesel
* Gasoline
* Electric

For Electric motors was implemented the design pattern Adapter because it have different features than other motors.

Foreach case exists 2 examples
### DIESEL
![image-3.png](./image-3.png)
### GASOLINE
![image-4.png](./image-4.png)
### ELECTRIC
![image-5.png](./image-5.png)
    *Electric motors have not an amount because use electricity
